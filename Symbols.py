import os
import pandas as pd

def Symbols(tickers, from_date=None, to_date=None):
  
    dir_db = "equities/sp500"
    all_files = os.listdir(dir_db)
    all_tickers = [file.split(".")[0] for file in all_files]
    #tickers = tickers if tickers in all_tickers else all_tickers
    
    for i in range(1, len(tickers)):
      if tickers[i] in all_tickers:
        next
      else:
        tickers[i] = None 
    
    file_name = os.path.join(dir_db, tickers[0] + ".csv")
    if len(tickers) == 1: 
      col_names = ['date', '_adj', '_close', '_raw', '_div', '_split']
      for i in range(1, len(col_names)):
        col_names[i] = tickers[0] + col_names[i]
      db = pd.read_csv(file_name)
      db.columns = col_names
    else: 
      db = pd.read_csv(file_name, usecols=['date', 'adj'])
      db.columns = ['date', tickers[0]]
      for i in range(1, len(tickers)):
        file_name = os.path.join(dir_db, tickers[i] + ".csv")
        new_db = pd.read_csv(file_name, usecols=['date', 'adj'])
        new_db.columns = ['date', tickers[i]]
        db = db.merge(new_db, on="date", how="left")  
      db = db.dropna()
        
    dates = pd.to_datetime(db['date'])
    # Filtraggio per una data di inizio o nessun filtro
    if from_date is not None:
       db = db.loc[dates >= pd.to_datetime(from_date), :]
    
    # Filtraggio per una data di fine o nessun filtro
    if to_date is not None:
        db = db.loc[dates <= pd.to_datetime(to_date), :]
    
    return db




