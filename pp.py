
from Symbols import Symbols

tickers = ["AAPL", "TSLA"]

Symbols(["AAPL"], from_date='2010-01-01', to_date='2010-01-10')
Symbols(["AAPL", "TSLA"])


dir_db = "equities/sp500"
all_files = os.listdir(dir_db)
all_tickers = [file.split(".")[0] for file in all_files]
#tickers = tickers if tickers in all_tickers else all_tickers
    
for i in range(1, len(tickers)):
  if tickers[i] in all_tickers:
    next
  else:
    tickers[i] = None

file_name = os.path.join(dir_db, tickers[0] + ".csv")
if len(tickers) == 1: 
  col_names = ['date', '_adj', '_close', '_raw', '_div', '_split']
  for i in range(1, len(col_names)):
    col_names[i] = tickers[0] + col_names[i]
  db = pd.read_csv(file_name)
  db.columns = col_names
else: 
  db = pd.read_csv(file_name, usecols=['date', 'adj'])
  db.columns = ['date', tickers[0]]
  for i in range(1, len(tickers)):
    file_name = os.path.join(dir_db, tickers[i] + ".csv")
    new_db = pd.read_csv(file_name, usecols=['date', 'adj'])
    new_db.columns = ['date', tickers[i]]
    db = db.merge(new_db, on="date", how="left")

    
db
 
 
 
